package com.mfahimi.cafebazaartask.Di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.mfahimi.cafebazaartask.Ui.Main.Detail.DetailViewModel
import com.mfahimi.cafebazaartask.Ui.Main.List.ListViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("unused")
@Module
abstract class ViewModelModule {
    @Binds
    @IntoMap
    @ViewModelKey(ListViewModel::class)
    abstract fun bindLisViewModel(userViewModel: ListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(DetailViewModel::class)
    abstract fun bindDetailViewModel(userViewModel: DetailViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: ApplicationViewModelFactory): ViewModelProvider.Factory
}

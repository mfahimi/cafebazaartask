package com.mfahimi.cafebazaartask.Ui.Main.Detail


import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.mfahimi.cafebazaartask.Data.Status
import com.mfahimi.cafebazaartask.Data.getStringMessage
import com.mfahimi.cafebazaartask.Ui.Base.BaseFragment
import com.mfahimi.cafebazaartask.Ui.Main.List.LoadingHolder.Companion.toVisibility
import kotlinx.android.synthetic.main.fragment_detail.*
import kotlinx.android.synthetic.main.layout_loading_page.*
import kotlinx.android.synthetic.main.toolbar.*
import java.text.SimpleDateFormat
import java.util.*


class DetailFragment : BaseFragment() {
    override fun getLayoutId(): Int = com.mfahimi.cafebazaartask.R.layout.fragment_detail

    lateinit var viewModel: DetailViewModel
    lateinit var args: DetailArgs

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(DetailViewModel::class.java)
        arguments?.let {
            args = arguments!!.getParcelable("data")!!
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        refresh.visibility = View.GONE
        viewsGroup.visibility = View.GONE

        toolbar_title.setText(args.title)

        viewModel.init(args.venueId)
        retry_button.setOnClickListener {
            setLoadingState(Status.LOADING)
            viewModel.retry(args.venueId)
        }

        viewModel.detail.observe(this, Observer {
            setLoadingState(it.status, it.message)
            when (it.status) {
                Status.SUCCESS -> {
                    val venue = it.data?.response?.venue
                    viewsGroup.visibility = View.VISIBLE
                    address.setText(venue?.location?.address)
                    city.setText(venue?.location?.city)
                    venue?.createdAt?.let {
                        val df = SimpleDateFormat("yyyy/MM/dd")
                        createdAt.text = df.format(Date(venue.createdAt))
                    }

                    val lat = it.data?.response?.venue?.location?.lat
                    val lng = it.data?.response?.venue?.location?.lng
                    showOnMap.setOnClickListener {
                        val gmmIntentUri = Uri.parse("geo:$lat,$lng?q=$lat,$lng(${args.title})")
                        val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
                        mapIntent.setPackage("com.google.android.apps.maps")
                        if (mapIntent.resolveActivity(context!!.packageManager) != null) {
                            startActivity(mapIntent);
                        }
                    }
                }
                Status.LOADING -> {
                    error_msg.setText("")
                }
                Status.ERROR -> {
                    error_msg.setText(it.error?.getStringMessage(context))
                }
            }
        })

    }


    private fun setLoadingState(status: Status, msg: String? = null) {
        if (status == Status.SUCCESS) {
            layout_loading_page.visibility = View.GONE
        } else {
            progress_bar.visibility = toVisibility(status == Status.LOADING)
            retry_button.visibility = toVisibility(status == Status.ERROR)
//            error_msg.visibility = toVisibility(msg != null )
//            error_msg.text = msg
        }
    }

    companion object {
        fun newInstance(args: DetailArgs): DetailFragment {
            return DetailFragment().apply {
                val bundle = Bundle()
                bundle.putParcelable("data", args)
                arguments = bundle
            }
        }
    }
}
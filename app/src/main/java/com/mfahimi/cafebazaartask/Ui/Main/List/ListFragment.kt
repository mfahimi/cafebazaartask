package com.mfahimi.cafebazaartask.Ui.Main.List

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.mfahimi.cafebazaartask.Data.Network.Model.Venue
import com.mfahimi.cafebazaartask.Data.NetworkState
import com.mfahimi.cafebazaartask.Data.getStringMessage
import com.mfahimi.cafebazaartask.Data.observeEvent
import com.mfahimi.cafebazaartask.R
import com.mfahimi.cafebazaartask.Service.GPSTracker
import com.mfahimi.cafebazaartask.UI.List.EnableLocationDialog
import com.mfahimi.cafebazaartask.Ui.Base.BaseFragment
import com.mfahimi.cafebazaartask.Util.PermissionUtils
import kotlinx.android.synthetic.main.fragment_list.*
import kotlinx.android.synthetic.main.toolbar.*
import javax.inject.Inject


class ListFragment @Inject constructor() : BaseFragment(), EnableLocationDialog.onClickListener {

    private val intentFilter = IntentFilter(GPSTracker.ACTION)


    override fun getLayoutId(): Int = R.layout.fragment_list

    lateinit var viewModel: ListViewModel
    private val service by lazy { Intent(context, GPSTracker::class.java) }
    val adapter by lazy { ListAdapter({ viewModel.retry() }, ::goToDetail) }
    var location: Location? = null
    val locationManager by lazy {
        context?.getSystemService(Context.LOCATION_SERVICE) as LocationManager
    }
    var mAlreadyLoaded: Boolean = false
    private lateinit var listener: ListFragmentListener
    override fun onAttach(activity: Activity) {
        super.onAttach(activity)
        if (activity is ListFragmentListener)
            listener = activity
        else
            throw RuntimeException("$activity must implement ListFragmentListener")
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putBoolean("mAlreadyLoaded", false);
        super.onSaveInstanceState(outState)

    }

    private fun goToDetail(venue: Venue) {
        listener.onListItemClick(venue)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(ListViewModel::class.java)
        toolbar_title.setText(resources.getString(R.string.nearbyVenue))
        initAdapter()
        observe()
        swipe_refresh.isEnabled = false
        progress_bar.visibility = View.GONE
        emptyList.visibility = View.VISIBLE
        if (savedInstanceState != null) {
            mAlreadyLoaded = true;
        }
        if (!mAlreadyLoaded) {
            checkPermission()
            mAlreadyLoaded = true
        }
        refresh.setOnClickListener { checkPermission() }

        if (PermissionUtils.hashPermission(context, PermissionUtils.LOCATION_PERMISSION) &&
            locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
        ) refresh.visibility = View.GONE
    }

    private fun checkPermission() {
        mAlreadyLoaded = true
        if (!PermissionUtils.hashPermission(context, PermissionUtils.LOCATION_PERMISSION))
            requestPermissions(
                PermissionUtils.LOCATION_PERMISSION,
                PermissionUtils.LOCATION_PERMISSION_ID
            )
        else
            checkIsLocationEnable()
    }

    private fun checkIsLocationEnable() {
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            context?.startService(service)
            progress_bar.visibility = View.VISIBLE
            swipe_refresh.isEnabled = true
            refresh.visibility = View.GONE
        } else {
            val ft = fragmentManager!!.beginTransaction()
            val prev = fragmentManager!!.findFragmentByTag("dialog")
            if (prev != null) {
                ft.remove(prev)
            }
            ft.addToBackStack(null)
            val dialogFragment = EnableLocationDialog()
            dialogFragment.setTargetFragment(this, 0)
            dialogFragment.show(ft, "dialog")
        }

    }

    private fun observe() {
        viewModel.posts.observe(this, Observer {
            showEmptyList(it?.size == 0)
            adapter.submitList(it)
            progress_bar.visibility = View.GONE
        })

        viewModel.networkState.observeEvent(this) {
            adapter.setNetworkState(it)
            it.apiError?.getStringMessage(context)?.let {
                Toast.makeText(context, it, Toast.LENGTH_SHORT).show()
            }
        }

        viewModel.refreshState.observeEvent(this) {
            swipe_refresh.isRefreshing = it == NetworkState.LOADING
            it.apiError?.getStringMessage(context)?.let {
                Toast.makeText(context, it, Toast.LENGTH_SHORT).show()

            }
        }
    }

    private fun initAdapter() {
        swipe_refresh.isEnabled = true
        list.itemAnimator?.removeDuration = 0
        list.itemAnimator?.addDuration = 0
        list.adapter = adapter

        swipe_refresh.setOnRefreshListener {
            if (location != null) {
                viewModel.refresh()
                emptyList.visibility = View.GONE
            }
        }
    }


    private fun showEmptyList(show: Boolean) {
        progress_bar.visibility = View.GONE
        if (show) {
            emptyList.setText(resources.getString(R.string.no_results))
            emptyList.visibility = View.VISIBLE
            list.visibility = View.GONE
        } else {
            emptyList.visibility = View.GONE
            list.visibility = View.VISIBLE
        }
    }

    override fun onResume() {
        super.onResume()
        context?.let { LocalBroadcastManager.getInstance(context!!).registerReceiver(receiver, intentFilter) }
    }

    override fun onDestroy() {
        context?.let { LocalBroadcastManager.getInstance(context!!).unregisterReceiver(receiver) }
        context?.stopService(service)
        super.onDestroy()
    }

    private val receiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            intent?.run {
                val lat = intent.getDoubleExtra("lat", 0.toDouble())
                val lng = intent.getDoubleExtra("lng", 0.toDouble())

                if (location == null) {//if location is null == first time we want to get data
                    location = Location(LocationManager.GPS_PROVIDER)
                    location!!.latitude = lat
                    location!!.longitude = lng
                    viewModel.showVenues(location!!)

                    //using this to prevent get data when location change is not more than we want
                } else if (location?.latitude != lat || location?.longitude != lng) {
                    location!!.latitude = lat
                    location!!.longitude = lng
                    adapter.submitList(null)
                    viewModel.refresh()
                } else
                    null
            }

        }

    }

    private val RQ_ENABLE_LOCATION: Int = 11

    override fun onClick(turnOnLocation: Boolean) {
        if (turnOnLocation)
            startActivityForResult(
                Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS),
                RQ_ENABLE_LOCATION
            )
        else
            viewModel.loadOldDataIfAvaliable()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RQ_ENABLE_LOCATION) {
            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                adapter.submitList(null)
                context?.startService(service)
                progress_bar.visibility = View.VISIBLE
                emptyList.visibility = View.GONE
                swipe_refresh.isEnabled = true
                refresh.visibility = View.GONE
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PermissionUtils.LOCATION_PERMISSION_ID) {
            if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                checkIsLocationEnable()
            } else {
                Toast.makeText(context, resources.getString(R.string.permissionDenied), Toast.LENGTH_SHORT).show()
            }

        }
    }

    interface ListFragmentListener {
        fun onListItemClick(venue: Venue)
    }
}
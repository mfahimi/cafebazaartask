package com.mfahimi.cafebazaartask.Repository

import android.app.Application
import androidx.lifecycle.LiveData
import com.mfahimi.cafebazaartask.Data.Local.AppDatabase
import com.mfahimi.cafebazaartask.Data.Network.ApiService
import com.mfahimi.cafebazaartask.Data.Network.Model.VenueDetail
import com.mfahimi.cafebazaartask.Data.Network.NetworkBoundResource
import com.mfahimi.cafebazaartask.Data.Resource
import com.mfahimi.cafebazaartask.Di.AppExecutors
import com.mfahimi.cafebazaartask.R
import javax.inject.Inject

class DetailRepository @Inject constructor(
    val application: Application,
    val apiService: ApiService,
    val appExecutors: AppExecutors,
    val database: AppDatabase
) {

    fun getVenueDetail(venueId: String): LiveData<Resource<VenueDetail>> {
        return object : NetworkBoundResource<VenueDetail, VenueDetail>(appExecutors) {
            override fun saveCallResult(item: VenueDetail) {
                database.getVenueDetailDao().insert(item)
            }

            override fun shouldFetch(data: VenueDetail?) = data == null

            override fun loadFromDb() = database.getVenueDetailDao().getVenuDetail(venueId = venueId)

            override fun createCall() = apiService.getDetail(
                venue_Id = venueId,
                clientID = application.applicationContext.resources.getString(R.string.client_ID),
                clientSecret = application.applicationContext.resources.getString(R.string.client_Secret)
            )
        }.asLiveData()
    }

    /**
     * for network-only requests we use this method instead of above, no any other changes needed
     */

//    fun getVenueDetail(venueId: String): LiveData<Resource<VenueDetail>> {
//        return object : NetworkResource<VenueDetail>() {
//            override fun createCall() = apiService.getDetail(
//                venue_Id = venueId,
//                clientID = application.applicationContext.resources.getString(R.string.client_ID),
//                clientSecret = application.applicationContext.resources.getString(R.string.client_Secret)
//            )
//        }.asLiveData()
//    }


}
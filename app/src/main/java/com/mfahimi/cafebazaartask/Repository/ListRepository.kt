package com.mfahimi.cafebazaartask.Repository

import android.app.Application
import android.location.Location
import androidx.annotation.MainThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.paging.LivePagedListBuilder
import com.mfahimi.cafebazaartask.Data.*
import com.mfahimi.cafebazaartask.Data.Local.AppDatabase
import com.mfahimi.cafebazaartask.Data.Network.ApiService
import com.mfahimi.cafebazaartask.Data.Network.BoundaryCallback
import com.mfahimi.cafebazaartask.Data.Network.Listing
import com.mfahimi.cafebazaartask.Data.Network.Model.Venue
import com.mfahimi.cafebazaartask.Data.Network.Model.explore
import com.mfahimi.cafebazaartask.Di.AppExecutors
import com.mfahimi.cafebazaartask.R
import io.reactivex.Single
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class ListRepository @Inject constructor(
    val application: Application,
    val apiService: ApiService,
    val executors: AppExecutors,
    val database: AppDatabase
) {
    companion object {
        private const val DEFAULT_NETWORK_PAGE_SIZE = 20
    }

    val pageSize: Int = DEFAULT_NETWORK_PAGE_SIZE
    private lateinit var boundaryCallback: BoundaryCallback
    @MainThread
    private fun refresh(location: Location): LiveData<Event<NetworkState>> {
        val networkState = MutableLiveData<Event<NetworkState>>()
        networkState.value = Event(NetworkState.LOADING)
        apiService.explore(
            application.applicationContext.resources.getString(R.string.client_ID),
            application.applicationContext.resources.getString(R.string.client_Secret),
            pageSize, 0, "${location.latitude},${location.longitude}"
        )
            .enqueue(object : Callback<explore> {
                override fun onFailure(call: Call<explore>, t: Throwable) {
                    networkState.postValue(Event(NetworkState.error(t.message, t.getApiError())))
                }

                override fun onResponse(call: Call<explore>, response: Response<explore>) {
                    executors.diskIO().execute {
                        database.runInTransaction {
                            database.getExploreDao().clearTable()
                            insertResultIntoDb(location, response.body()?.response?.groups!![0])
                        }
                    }
                    networkState.postValue(Event(NetworkState.LOADED))
                    boundaryCallback.lastRequestedPage = 1
                }

            })

        return networkState
    }

    private fun insertResultIntoDb(location: Location, body: explore.Response.Group?) {
        body?.items?.let { posts ->
            database.runInTransaction {
                val items = posts.mapIndexed { index, child ->
                    child.venue
                }
                items.forEach {
                    it.search_lat = location.latitude
                    it.search_lng = location.longitude
                }
                database.getExploreDao().insertAll(items)
            }
        }
    }

    fun explore(location: Location): Listing<Venue> {

        // create a boundary callback which will observe when the user reaches to the edges of
        // the list and update the database with extra data.
        boundaryCallback = BoundaryCallback(
            webservice = apiService,
            location = location,
            handleResponse = this::insertResultIntoDb,
            ioExecutor = executors.networkIO(),
            networkPageSize = pageSize,
            clientId = application.applicationContext.resources.getString(R.string.client_ID),
            secredId = application.applicationContext.resources.getString(R.string.client_Secret)
        )

        val refreshTrigger = MutableLiveData<Unit>()
        val refreshState = Transformations.switchMap(refreshTrigger) {
            refresh(location)
        }


        val livePagedList =
            LivePagedListBuilder(
                database.getExploreDao().getData(),
                pageSize
            )
                .setBoundaryCallback(boundaryCallback)
                .build()

        return Listing(
            pagedList = livePagedList,
            networkState = boundaryCallback.networkState,
            retry = {
                boundaryCallback.helper.retryAllFailed()
            },
            refresh = {
                refreshTrigger.value = null
            },
            refreshState = refreshState
        )
    }

    fun getLastLocation(): Single<com.mfahimi.cafebazaartask.Data.Local.Location> {
        return database.getExploreDao().getLocation()
    }

}
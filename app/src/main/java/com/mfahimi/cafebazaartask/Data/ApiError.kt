package com.mfahimi.cafebazaartask.Data

import android.content.Context
import com.google.gson.JsonParseException
import com.mfahimi.cafebazaartask.R
import org.json.JSONException
import retrofit2.Response
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.util.concurrent.TimeoutException

/**
 * This enum class could be different for each application based on server response and errors that might happen
 * here is just simple and common errors type
 * On UI we can react based on this error type
 */
enum class ApiError {
    ERROR_204, ERROR_400, ERROR_401, ERROR_404, ERROR_405, ERROR_429,
    ERROR_500, ERROR_TIME_OUT, ERROR_DISCONNECTED, ERROR_PARSE, ERROR_UNKNOWN, ERROR_NO_CONTENT
}

/**
 * create a readable error message for user based on error type
 * return null if you don't want to show any message
 */
fun ApiError.getStringMessage(context: Context?): String? {
    context?.let {
        return when (this) {
            ApiError.ERROR_204 -> context.resources.getString(R.string.ERROR_204)
            ApiError.ERROR_404 -> context.resources.getString(R.string.ERROR_404)
            ApiError.ERROR_429 -> context.resources.getString(R.string.ERROR_429)
            ApiError.ERROR_500 -> context.resources.getString(R.string.ERROR_500)
            ApiError.ERROR_DISCONNECTED -> context.resources.getString(R.string.ERROR_DISCONNECTED)
            ApiError.ERROR_UNKNOWN -> context.resources.getString(R.string.ERROR_UNKNOWN)
            else -> null
        }
    }
    return null
}

/**
 * Return error type based on retrofit throwable.
 */

fun Throwable.getApiError(): ApiError {
    return when (this) {
        is UnknownHostException -> ApiError.ERROR_DISCONNECTED
        is JsonParseException, is JSONException -> ApiError.ERROR_PARSE
        is TimeoutException, is SocketTimeoutException -> ApiError.ERROR_TIME_OUT
        else -> ApiError.ERROR_UNKNOWN
    }
}

/**
 * return error type based on retrofit response.
 * it is used to get error type when response code is not 200
 */

fun <T> Response<T>.getApiError(): ApiError {
    return when (this.code()) {
        400 -> ApiError.ERROR_400
        401 -> ApiError.ERROR_401
        404 -> ApiError.ERROR_404
        405 -> ApiError.ERROR_405
        429 -> ApiError.ERROR_429
        500 -> ApiError.ERROR_500
        else -> ApiError.ERROR_UNKNOWN
    }
}
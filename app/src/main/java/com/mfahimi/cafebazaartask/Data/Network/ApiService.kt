package com.mfahimi.cafebazaartask.Data.Network

import androidx.lifecycle.LiveData
import com.mfahimi.cafebazaartask.Data.Network.Model.VenueDetail
import com.mfahimi.cafebazaartask.Data.Network.Model.explore
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    @GET("venues/explore?sortByDistance=true&v=20190416")
    fun explore(
        @Query("client_id") clientID: String,
        @Query("client_secret") clientSecret: String,
        @Query("limit") limit: Int,
        @Query("offset") offset: Int,
        @Query("ll") ll: String
    ): Call<explore>

    @GET("venues/{VENUE_ID}?v=20190416")
    fun getDetail(
        @Path("VENUE_ID") venue_Id: String,
        @Query("client_id") clientID: String,
        @Query("client_secret") clientSecret: String
    ): LiveData<ApiResponse<VenueDetail>>
}
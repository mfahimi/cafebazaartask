package com.mfahimi.cafebazaartask.Data.Local

data class Location(val lat: Double, val lng: Double)
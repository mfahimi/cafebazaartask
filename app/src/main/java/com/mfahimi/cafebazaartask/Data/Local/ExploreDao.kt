package com.mfahimi.cafebazaartask.Data.Local

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Query
import com.mfahimi.cafebazaartask.Data.Network.Model.Venue
import io.reactivex.Single

@Dao
abstract class ExploreDao : BaseDao<Venue> {

    @Query("DELETE FROM venue")
    abstract fun clearTable()

    @Query("SELECT * FROM venue WHERE search_lat=:lat AND search_lng=:lng")
    abstract fun getData(lat: Double, lng: Double): DataSource.Factory<Int, Venue>

    @Query("SELECT * FROM venue")
    abstract fun getData(): DataSource.Factory<Int, Venue>

    @Query("SELECT * FROM venue LIMIT :limit OFFSET :offset")
    abstract fun getData(limit: Int, offset: Int): LiveData<Venue>

    @Query("SELECT search_lat as lat,search_lng as lng FROM VENUE LIMIT 1")
    abstract fun getLocation(): Single<Location>
}
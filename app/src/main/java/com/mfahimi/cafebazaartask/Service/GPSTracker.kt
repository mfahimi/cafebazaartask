package com.mfahimi.cafebazaartask.Service

import android.annotation.SuppressLint
import android.app.Service
import android.content.Context
import android.content.Intent
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.os.IBinder
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.mfahimi.cafebazaartask.Util.roundLocation


class GPSTracker : Service(), LocationListener {
    private val TWO_MINUTES: Long = 1000 * 60 * 2

    companion object {
        const val ACTION = "com.mfahimi.cafebazaartask.Service.GPSTracker"
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        getUserLocation()
        return START_REDELIVER_INTENT
    }

    // flag for GPS status
    var isGPSEnabled = false

    // flag for network status
    var isNetworkEnabled = false

    var canGetLocation = false

    var location: Location? = null // location
    var lat: Double = 0.toDouble() // lat
    var lng: Double = 0.toDouble() // lng

    // The minimum distance to change Updates in meters
    private val MIN_DISTANCE_CHANGE_FOR_UPDATES: Long = 100 // 100 meters

    // The minimum time between updates in milliseconds
    private val MIN_TIME_BW_UPDATES = (1000 * 20).toLong() // 10 seconds

    // Declaring a Location Manager
    protected var locationManager: LocationManager? = null


    /**
     * Function to get lat
     */
    fun getLatitude(): Double {
        location?.let { lat = it.latitude }
        return lat
    }

    /**
     * Function to get lng
     */
    fun getLongitude(): Double {
        location?.let { lng = it.longitude }
        return lng
    }

    /**
     * Function to check if best network provider
     * @return boolean
     */
    fun canGetLocation(): Boolean {
        return this.canGetLocation
    }

    /**
     * Stop using GPS listener
     * Calling this function will stop using GPS in your app
     */
    fun stopUsingGPS() {
        if (locationManager != null) {
            locationManager!!.removeUpdates(this@GPSTracker)
        }
    }


    //depend on use case of application its bette to use on provider for location update
    // it reduces battery usage at a potential cost of accuracy.
    @SuppressLint("MissingPermission")
    fun getUserLocation(): Location? {
        try {
            locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
            //if we want to get last known location or get location fast
//            locationManager?.let {
//                val lastKnownLocation: Location = locationManager!!.getLastKnownLocation(LocationManager.GPS_PROVIDER)
//                onLocationChanged(lastKnownLocation)
//            }

            // getting GPS status
            isGPSEnabled = locationManager!!
                .isProviderEnabled(LocationManager.GPS_PROVIDER)

            // getting network status
            isNetworkEnabled = locationManager!!
                .isProviderEnabled(LocationManager.NETWORK_PROVIDER)

            if (!isGPSEnabled && !isNetworkEnabled) {
                // no network provider is enabled
            } else {
                this.canGetLocation = true
                // First get location from Network Provider
                if (isNetworkEnabled) {
                    locationManager!!.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER,
                        MIN_TIME_BW_UPDATES,
                        MIN_DISTANCE_CHANGE_FOR_UPDATES.toFloat(), this
                    )
                    if (locationManager != null) {
                        location = locationManager!!
                            .getLastKnownLocation(LocationManager.NETWORK_PROVIDER)
                        if (location != null) {
                            lat = location!!.latitude
                            lng = location!!.longitude
                        }
                    }
                }
                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    if (location == null) {
                        locationManager!!.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES.toFloat(), this
                        )
                        if (locationManager != null) {
                            location = locationManager!!
                                .getLastKnownLocation(LocationManager.GPS_PROVIDER)
                            if (location != null) {
                                lat = location!!.latitude
                                lng = location!!.longitude
                            }
                        }
                    }
                }

                location?.let {
                    onLocationChanged(location!!)
                }
            }

        } catch (e: Exception) {
            e.printStackTrace()
        }

        return location
    }

    /** Determines whether one Location reading is better than the current Location fix
     * @param location The new Location that you want to evaluate
     * @param currentBestLocation The current Location fix, to which you want to compare the new one
     */
    fun isBetterLocation(location: Location, currentBestLocation: Location?): Boolean {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true
        }

        // Check whether the new location fix is newer or older
        val timeDelta: Long = location.time - currentBestLocation.time
        val isSignificantlyNewer: Boolean = timeDelta > TWO_MINUTES
        val isSignificantlyOlder: Boolean = timeDelta < -TWO_MINUTES

        when {
            // If it's been more than two minutes since the current location, use the new location
            // because the user has likely moved
            isSignificantlyNewer -> return true
            // If the new location is more than two minutes older, it must be worse
            isSignificantlyOlder -> return false
        }

        // Check whether the new location fix is more or less accurate
        val isNewer: Boolean = timeDelta > 0L
        val accuracyDelta: Float = location.accuracy - currentBestLocation.accuracy
        val isLessAccurate: Boolean = accuracyDelta > 0f
        val isMoreAccurate: Boolean = accuracyDelta < 0f
        val isSignificantlyLessAccurate: Boolean = accuracyDelta > 200f

        // Check if the old and new location are from the same provider
        val isFromSameProvider: Boolean = location.provider == currentBestLocation.provider

        // Determine location quality using a combination of timeliness and accuracy
        return when {
            isMoreAccurate -> true
            isNewer && !isLessAccurate -> true
            isNewer && !isSignificantlyLessAccurate && isFromSameProvider -> true
            else -> false
        }
    }

    override fun onLocationChanged(newLocation: Location) {
        if (isBetterLocation(newLocation, location)) {
            location = newLocation
        }
        location?.let {
            val locationIntent = Intent(ACTION)
            locationIntent.putExtra("lat", location!!.latitude.roundLocation())
            locationIntent.putExtra("lng", location!!.longitude.roundLocation())
            LocalBroadcastManager.getInstance(this).sendBroadcast(locationIntent)
        }
    }

    override fun onProviderDisabled(provider: String) {
        stopUsingGPS()
    }

    override fun onProviderEnabled(provider: String) {
        getUserLocation()
    }

    override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}

    override fun onBind(intent: Intent?): IBinder? = null

    override fun onDestroy() {
        stopUsingGPS()
        super.onDestroy()
    }

}